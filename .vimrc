""SOURCES
"
source $HOME/.vim/plug-config/coc.vim



" NVIM/VIM SETTERS
" ----------------------------------------------------------------------------------------------------------------------------------

"hightligning text
syntax on

"Set number in lines
"set number 

"for plugins load correctly
filetype plugin indent on

"Set max 4 spaces for tabs
set tabstop=4
set shiftwidth=4


" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
" Initialize plugin system
"
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', { 'branch': 'release'}
Plug 'StanAngeloff/php.vim'
"Plug 'valloric/youcompleteme'
Plug 'vim-airline/vim-airline'
Plug 'Yggdroot/indentLine'
Plug 'w0rp/ale'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdcommenter'
Plug 'mxw/vim-jsx'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

"
"OneDark theme
"
Plug 'joshdick/onedark.vim'

"
" Dracula Theme
"

Plug 'dracula/vim'

call plug#end()

"------------------------------------------------------------------------------------------------------------------------------------


"PLUGINS SETTERS

"Color themes
"colorscheme onedark
"colorscheme dracula 
if has('joshdick/onedark.vim')
    colorscheme onedark  
endif



"indent lines
let g:indentLine_fileTypeExclude = ['text', 'sh', 'help', 'terminal']
let g:indentLine_bufNameExclude = ['NERD_tree.*', 'term:.*']

" Mostrar mejor mensajes de error ALE
" :ALEFixSuggest
" :ALEFix
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

"NerdCommenter
"
let g:NERDSpaceDelims = 1  " Agregar un espacio después del delimitador del comentario
let g:NERDTrimTrailingWhitespace = 1  " Quitar espacios al quitar comentario

